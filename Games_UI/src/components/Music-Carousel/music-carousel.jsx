import React, { Component } from 'react';
import { AutoRotatingCarousel } from 'material-auto-rotating-carousel';
import { Slide } from 'material-auto-rotating-carousel';
import Button from '@material-ui/core/Button';
import red from '@material-ui/core/colors/red';

class MusicCarousel extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (this.props.slides.length > 0) {
      return (
        <div style={{ position: 'relative', width: '100%', height: 50 }}>
          <Button onClick={() => this.setState({ open: true })}>Youtube Videos</Button>
          <AutoRotatingCarousel
            open={this.state.open}
            onClose={() => this.setState({ open: false })}
            onStart={() => this.setState({ open: false })}
            style={{ position: 'absolute' }}
            autoplay={false}
          >
            {this.props.slides.map((slide, index) => {
              return (
              <Slide
                media={<iframe height="480" width="500" src={slide.src} title={slide.title}></iframe>}  
                mediaBackgroundStyle={{ backgroundColor: red[400] }}
                style={{ backgroundColor: red[600] }}
                title={slide.artistName}
                subtitle={slide.title}
                key={index}
              />
              )
            })}
          </AutoRotatingCarousel>
        </div>
      )
    }
    else return null;
  }
}

export default MusicCarousel;