import React, { Component } from "react";
import { Route, Link, BrowserRouter as Router, Switch } from "react-router-dom";
import Artist from './Artist/artist';
import Home from './Home/home';
import GameSection from './Games/game-section';
import MusicSection from './Music/music-section';
import NotFound from './notfound';

import './routing-section.css';

class RoutingSection extends Component {
  state = {};
  
  render() {
    return (
      <div>
        <Router>
          <div className="routing-section">
            <ul className="nav nav-pills">
              <li className="nav-item">
                <Link to="/">Home</Link>
              </li>
              <li className="nav-item">
                <Link to="/game-section">Game List</Link>
              </li>
              <li className="nav-item">
                <Link to="/music-section">Music List</Link>
              </li>
            </ul>

            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/game-section" component={GameSection} />
              <Route path="/music-section" component={MusicSection} />
              <Route path="/artist/:id" component={Artist} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Router>
      </div>
    )
  }
}

export default RoutingSection; 