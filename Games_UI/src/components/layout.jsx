import React, { Component } from "react";
import { PageHeader } from 'react-bootstrap';
import RoutingSection from './routing-section';

import './layout.css';

class Layout extends Component {
	state = {};

	render() {
		return (
			<div>
				<div className="backgroundStyle">
					<div className="titleSection">
						<PageHeader>
							Game and Music Tracker
						</PageHeader>
					</div>
					<RoutingSection></RoutingSection>
				</div>
			</div>
		)
	};
}

export default Layout; 
