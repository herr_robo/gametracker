import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import 'typeface-roboto';
import imageLoader from './images';
import Image from '../Image/image'

class AlbumSection extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  generateAlbumElement(albums) {
    let element = albums.map((album, index) => {

      let loadedImage = imageLoader(album.albumId);
      return (
        <TableRow key={index}>
          <TableCell align="left" component="th" scope="row">{album.albumName}</TableCell>
          <TableCell align="right">{album.albumType}</TableCell>
          <TableCell align="right">{album.albumDuration}</TableCell>
          <TableCell align="right">{album.dateReleased}</TableCell>
          <TableCell align="right">
            <Image path={loadedImage} name={album.albumName}/>
          </TableCell>
        </TableRow>
      )
    })
    return element;
  }

  render() {
    if (this.props.albums && this.props.albums.length > 0) {
      let albumElement = this.generateAlbumElement(this.props.albums);
      return(
        <div>
          <TableContainer component={Paper}>
            <Table aria-label="Album Information Table">
              <TableHead>
                <TableRow>
                  <TableCell align="left">Name</TableCell>
                  <TableCell align="right">Type</TableCell>
                  <TableCell align="right">Duration</TableCell>
                  <TableCell align="right">Date Released</TableCell>
                  <TableCell align="right">Album Image</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {albumElement}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      )
    }
    else {
      return <div> no albums </div>
    }
  }
}

export default AlbumSection;