import React, { Component } from 'react';
import Container from '@material-ui/core/Container';
import AlbumSection from './album-section';
import MusicCarousel from '../Music-Carousel/music-carousel';
import videoLoader from './artists-videos';

class Artist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artistId: 0,
      artist: [],
      videos: [],
      isLoading: false
    }
    this.getArtist = this.getArtist.bind(this);
  }

	componentDidMount() {
    const { params } = this.props.match;
    let videos = videoLoader(params.id);
    this.setState({ isLoading: true, artistId: params.id, videos: videos });
    this.getArtist(params.id);
	}

  getArtist(id) {
    fetch('https://localhost:44374/api/artist/' + id + '/getAll')
    .then(response => {
      if(response.ok) {
        return response.json();
      }
      else {
        throw new Error(response.statusText);
      }
    })
    .then(data => {
      this.setState({isLoading: false, artist: data });
    })
    .catch(error => {
      this.setState({ artist: [] });
      console.log('Error: ', error);
    })
  }


  render() {
    const { params } = this.props.match;
    if (params.id < 1) {
      return(
        <div>
          No artist found
        </div>
      )
    }

    return(
      <div>
        <Container maxWidth="lg">
          <MusicCarousel slides={this.state.videos}/>
          <AlbumSection albums={this.state.artist.albums}/>
        </Container>
      </div>
    )
  };
}

export default Artist;
