
const images = [
  { albumId: 1, src: '/images/albums/Chvrches/Chvrches_The_Bones_of_What_You_Believe.png' },
  { albumId: 2, src: '/images/albums/Chvrches/Chvrches_Every_Open_Eye.png' },
  { albumId: 3, src: '/images/albums/Chvrches/Chvrches_Love_Is_Dead.png' },
  { albumId: 4, src: '/images/albums/Chvrches/Chvrches_Recover_EP.jpg' },
  { albumId: 5, src: '/images/albums/Chvrches/Chvrches_EP.jpg' },
  { albumId: 6, src: '/images/albums/Chvrches/Chvrches_Under_The_Tide_EP.jpg' },
  { albumId: 7, src: '/images/albums/Chvrches/Chvrches_Hansa_Session_EP.jpg' },
  { albumId: 8, src: '/images/albums/TheNational/The_National_The_National.png' },
  { albumId: 9, src: '/images/albums/TheNational/The_National_Sad_Songs.jpg' },
  { albumId: 10, src: '/images/albums/TheNational/The_National_Alligator.jpg' },
  { albumId: 11, src: '/images/albums/TheNational/The_National_Boxer.jpg' },
  { albumId: 12, src: '/images/albums/TheNational/The_National_High_Violet.jpg' },
  { albumId: 13, src: '/images/albums/TheNational/The_National_Trouble_Will_Find_me.jpg' },
  { albumId: 14, src: '/images/albums/TheNational/The_National_Sleep_Well_Beast.jpg' },
  { albumId: 15, src: '/images/albums/TheNational/The_National_I_Am_Easy_to_Find.png' },
  { albumId: 16, src: '/images/albums/TheNational/The_National_Cherry_Tree.jpg' },
  { albumId: 17, src: '/images/albums/TheNational/The_National_The_Virginia_EP.jpg' },
  { albumId: 18, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Jimmy_Eat_World.jpg' },
  { albumId: 19, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Static_Prevails.jpg' },
  { albumId: 20, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Clarity.jpg' },
  { albumId: 21, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Bleed_American.jpg' },
  { albumId: 22, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Futures.jpg' },
  { albumId: 23, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Chase_This_Light.jpg' },
  { albumId: 24, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Invented.jpg' },
  { albumId: 25, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Damage.jpg' },
  { albumId: 26, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Integrity_Blues.jpg' },
  { albumId: 32, src: '/images/albums/JimmyEatWorld/Jimmy_Eat_World_Stay_On_My_Side_Tonight.jpg' }
];

function imageLoader(id) {
  let foundImage = images.filter(image => image.albumId === id);
  return foundImage;
}

export default imageLoader;