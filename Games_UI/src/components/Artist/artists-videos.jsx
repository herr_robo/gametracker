
const videos = [
  { artistId: 1, videoId: 1, artistName: "CHVRCHES", src: 'https://www.youtube.com/embed/HHnjuUVgSJ4', title: 'Marshmello - Here With Me Feat. CHVRCHES' },
  { artistId: 1, videoId: 2, artistName: "CHVRCHES", src: 'https://www.youtube.com/embed/S8HI6-Ij3LI', title: 'We Sink' },
  { artistId: 1, videoId: 3, artistName: "CHVRCHES", src: 'https://www.youtube.com/embed/QpFXXPruuqU', title: 'Clearest Blue' },
  { artistId: 2, videoId: 4, artistName: "The National", src: 'https://www.youtube.com/embed/bFnA-8H-5lo', title: 'Don\'t Swallow the Cap' },
  { artistId: 2, videoId: 5, artistName: "The National", src: 'https://www.youtube.com/embed/Ipx8qWt2fVA', title: 'About Today' },
  { artistId: 2, videoId: 6, artistName: "The National", src: 'https://www.youtube.com/embed/A-Tod1_tZdU', title: 'I Need My Girl' },
  { artistId: 3, videoId: 7, artistName: "Jimmy Eat World", src: 'https://www.youtube.com/embed/PPtFp1qRFcE', title: 'A Praise Chorus' },
  { artistId: 3, videoId: 8, artistName: "Jimmy Eat World", src: 'https://www.youtube.com/embed/OGP-0-8yL4k', title: 'Let It Happen' },
  { artistId: 3, videoId: 9, artistName: "Jimmy Eat World", src: 'https://www.youtube.com/embed/FIhxSnAHFeA', title: 'Sure and Certain' }
];

function videoLoader(id) {
  let foundVideos = videos.filter(video => video.artistId === parseInt(id));
  return foundVideos;
}

export default videoLoader;