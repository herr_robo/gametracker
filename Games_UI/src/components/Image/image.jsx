import React, { Component } from 'react';

class Image extends Component {
  render() {
    if (this.props.path.length > 0 && this.props.path[0].src) {
      return (
        <img src={this.props.path[0].src} height="200" width="200" alt={this.props.name}/>
      )
    }
    else {
      return <div> no image </div>;
    }
  }
}

export default Image;