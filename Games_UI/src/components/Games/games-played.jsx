import React, { Component } from "react";
import './games-played.css';

class GamesPlayed extends Component {
	state = {};

	render() {
    if (this.props.showHasPlayed) { 
      var playedListElement = this.props.gamesPlayed.map((game, index) => {
        return (
          <li key={index}>
            {game.title}
          </li>
        )
      })
      return (
        <div>
          <div className="games-played-layout">
            Component that shows games played
            <ul>
              { playedListElement }
            </ul>
          </div>
        </div>
      )
    }

    else {
      return null;
    }

	};
}

export default GamesPlayed;
