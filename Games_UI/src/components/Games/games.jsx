import React, { Component } from "react";
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import './games.css';

class Games extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			isLoading: false,
			games: [],
			gameCount: 0
		};
	}

	generateGameList(games) {
		let gameList = games.map((game, index) => {
			return (
				<ListGroupItem key={index}>
					<label>{game.title}</label>
					<div>
						<label>Description:</label> {game.description}
					</div>
					<div>
						<label>Developed by:</label> {game.developer}
					</div>
					<div>
						<label>Played game: </label> {game.played ? 'Yes' : 'No'}
					</div>
					<hr/>
					<div>
						<div className="btn-group" role="group" aria-label="Row Group">
							<button 
								type="button" className="btn btn-secondary" 
								onClick={this.clickEditHandler(game)}
							>
								Edit
							</button>
							<button
								type="button" className="btn btn-secondary" 
								onClick={this.clickDeleteHandler(game)}
							>
								Delete
							</button>
						</div>
					</div>
				</ListGroupItem>
			)
		})
		return gameList;
	}

	clickEditHandler = game => () => {
		this.props.editRow(game)
	};

	clickDeleteHandler = game => () => {
		this.props.deleteRow(game);
	};

	render() {
		if (this.state.isLoading || this.props.gameList.length === 0) {
			return null;
		}

		let gameListElement = this.generateGameList(this.props.gameList);
		return (
			<div>
				<ListGroup>
					{ gameListElement }
				</ListGroup>
			</div>
		)
	};
}

export default Games; 
