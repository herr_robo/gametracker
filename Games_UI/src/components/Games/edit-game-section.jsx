import React, { Component } from "react";
import './edit-game-section.css';

class EditGameSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      id: '',
      title: '',
      description: '',
      developer: '',
      played: false
    };

    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeDeveloper = this.handleChangeDeveloper.bind(this);
    this.handleChangePlayed = this.handleChangePlayed.bind(this);
    this.editGame = this.editGame.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }

  inputStyle = {
    width: '100%'
  };

  textareaStyle = {
    width: '100%',
    resize: 'none'
  };

  handleChangeTitle(event) {
    this.setState({'title': event.target.value});
  }

  handleChangeDescription(event) {
    this.setState({'description': event.target.value});
  }

  handleChangeDeveloper(event) {
    this.setState({'developer': event.target.value});
  }

  handleChangePlayed(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({'played': value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.editGame();
  }

  handleClear() {
    this.setState({
      title: '',
      description: '',
      developer: '',
      played: false
    });
  }

  handleCancel() {
    this.handleClear();
    this.setState({id: ''});
    this.props.onEditGameChange(null);
  }

  editGame() {
    var game = {
      'id': this.props.editId,
      'title': this.state.title || this.props.editTitle,
      'description': this.state.description || this.props.editDescription,
      'developer': this.state.developer || this.props.editDeveloper,
      'played': this.state.played
    }

    return fetch('https://localhost:44374/api/games/' + game.id,
      {
        method: 'PUT', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(game)
      })
      .then(response => {
        response.json();

        this.setState({
          id: '',
          title: '',
          description: '',
          developer: '',
          played: false
        });
        this.props.onEditGameChange(game);
      })
      .catch(error => console.error(error));
  }

  render() {
    if (this.props.showEditMode) {
      return (
        <div>
          <div className="edit-layout">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label>Game title</label>
                <div>
                  <input 
                    type="text" 
                    required 
                    value={this.state.title} 
                    style={this.inputStyle} 
                    onChange={this.handleChangeTitle} />
                </div>
              </div>
              <div className="form-group">
                <label>Game description</label>
                <div>
                  <textarea 
                    type="text" 
                    required 
                    value={this.state.description} 
                    style={this.textareaStyle} 
                    onChange={this.handleChangeDescription} />
                </div>
              </div>
              <div className="form-group">
                <label>Game developer</label>
                <div>
                  <input 
                    type="text" 
                    required 
                    value={this.state.developer} 
                    style={this.inputStyle} 
                    onChange={this.handleChangeDeveloper} />
                </div>
              </div>
              <div className="form-group">
                <label>Game Played?</label>
                <div>
                  <input 
                    name="played"
                    type="checkbox" 
                    checked={this.state.played} 
                    onChange={this.handleChangePlayed} />
                </div>
              </div>
              <div className="btn-group" role="group" aria-label="Edit Game Group">
                <input type="submit" value="Update" color="primary" className="btn btn-primary" />
                <button type="button" className="btn btn-secondary" onClick={this.handleClear}>
                  Clear
                </button>
                <button type="button" className="btn btn-secondary" onClick={this.handleCancel}>
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      )
    }
    else {
      return null;
    }

  }
}

export default EditGameSection;