import React, { Component } from "react";
import EditGameSection from './edit-game-section';
import Games from './games';
import NewGameModal from './Modals/newGameModal';
import DeleteGameModal from './Modals/deleteGameModal';
import GamesPlayed from "./games-played";
import './game-section.css';


class GameSection extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			games: [],
			editMode: false,
			editId: '', 
			editTitle: '',
			editDescription: '',
			editDeveloper: '',
			editPlayed: '',
			showDeleteModal: false,
			deleteGameName: '',
			deleteGameId: '',
			showHasPlayed: false,
			gamesPlayed: []
		};
		
		this.handleAddedGameRecord = this.handleAddedGameRecord.bind(this);
		this.handleEditGameRecord = this.handleEditGameRecord.bind(this);
		this.handleDeleteModalClose = this.handleDeleteModalClose.bind(this);
		this.editRow = this.editRow.bind(this);
		this.deleteRow = this.deleteRow.bind(this);
	}

	getGameList() {
		fetch('https://localhost:44374/api/games')
			.then(response => {
				if (response.ok) {
					return response.json();
				}
				else {
					throw new Error(response.statusText);
				}

			})
			.then(data => {
				this.setState({ games: data, isLoading: false });
				let playedList = [];
				data.forEach(game => {
					if(game.played === true) {
						playedList.push(game);
					}
				});
				if (playedList.length > 0) {
					this.setState({ 
						showHasPlayed: true,
						gamesPlayed: playedList
					});
				}

			})
			.catch(error => {
				this.setState({ games: [] });
				console.log('Error: ', error);
			});
	}

	handleAddedGameRecord(newGame) {
		let newGameList = this.state.games;
		newGameList.push(newGame);
		console.log('newGameList', newGameList);
		this.setState({ games: newGameList });
	}

	editRow(game) {
		this.setState({ 
			editMode: true,
			editId: game.id,
			editTitle: game.title, 
			editDescription: game.description,
			editDeveloper: game.developer,
			editPlayed: game.played
		});
	}

	deleteRow(game) {
		this.setState({
			showDeleteModal: true, 
			deleteGameName: game.title,
			deleteGameId: game.id
		});
	}

	handleEditGameRecord(editedGame) {
		if (editedGame) {
			let updatedGameList = this.state.games;
			updatedGameList.forEach(game => {
				if (editedGame.id === game.id) {
					game.title = editedGame.title;
					game.description = editedGame.description;
					game.developer = editedGame.developer;
					game.played = editedGame.played;
				}
			})
			let gamesPlayedList = updatedGameList.filter(playedGame => playedGame.played === true);
			this.setState({ editMode: false, games: updatedGameList, gamesPlayed: gamesPlayedList });
		} else {
			this.setState({ editMode: false});
		}
	}

	handleDeleteModalClose(deletedGame) {
		if (deletedGame) {
			let currentGameList = this.state.games;
			let newGameList = currentGameList.filter(game => game.id !== deletedGame);
			this.setState({ games: newGameList });
		}
		this.setState({showDeleteModal: false});
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		this.getGameList();
	}

	render() {
		return (
			<div>
				<NewGameModal 
					onNewGameAddedChange={this.handleAddedGameRecord} 
				/>
				<DeleteGameModal 
					showDeleteModal={this.state.showDeleteModal} 
					deleteGameName={this.state.deleteGameName}
					deleteGameId={this.state.deleteGameId}
					onDeleteClose={this.handleDeleteModalClose}
				/>
				<div className="container-fluid">
					<div className="row">
						<div className="col-sm-6">
							<Games 
								gameList={this.state.games} 
								editRow={this.editRow} 
								deleteRow={this.deleteRow}
							/>
						</div>
						<div className="col-sm-6">
							<EditGameSection 
								onEditGameChange={this.handleEditGameRecord}
								editId={this.state.editId}
								title={this.state.editTitle} 
								description={this.state.editDescription}
								developer={this.state.editDeveloper}
								played={this.state.editPlayed}
								showEditMode={this.state.editMode}
							/>
							<GamesPlayed 
								gamesPlayed={this.state.gamesPlayed} 
								showHasPlayed={this.state.showHasPlayed}/>
						</div>
					</div>
				</div>
			</div>
		)
	};
}

export default GameSection;