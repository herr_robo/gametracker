import React, { Component } from "react";
import { Modal } from 'react-bootstrap';

class DeleteGameModal extends Component {

  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.deleteGame = this.deleteGame.bind(this);
  }

  handleDelete() {
    this.deleteGame();
  }

  handleClose() {
    this.props.onDeleteClose(undefined);
  }

  deleteGame() {
    return fetch('https://localhost:44374/api/games/' + this.props.deleteGameId,
    {
      method: 'DELETE', 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then(response => {
      this.props.onDeleteClose(this.props.deleteGameId);
    })
    .catch(error => console.error(error));
  }

  render() {
    return (
      <div>
        <Modal show={this.props.showDeleteModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Delete game?</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure you want to delete {this.props.deleteGameName}?</Modal.Body>
          <Modal.Footer>
            <button type="button" className="btn btn-primary" onClick={this.handleDelete}>
              Delete Game
            </button>
            <button type="button" className="btn btn-secondary" onClick={this.handleClose}>
              Cancel
            </button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  };
}

export default DeleteGameModal;