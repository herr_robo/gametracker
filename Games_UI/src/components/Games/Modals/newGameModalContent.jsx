import React, { Component } from "react";

class NewGameModalContent extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  inputStyle = {
    width: '100%'
  };

  textareaStyle = {
    width: '100%',
    resize: 'none'
  };

  render() {
    return (
      <div>
        <div>
          <label>Game Title</label>
          <div>
            <input type="text" style={this.inputStyle}></input>
          </div>
        </div>
        <div>
          <label>Game Summary</label>
          <div>
            <textarea type="text" style={this.textareaStyle}/>
          </div>
        </div>
        <div>
          <label>Game Developer</label>
          <div>
            <input type="text" style={this.inputStyle}></input>
          </div>
        </div>
      </div>
    )
  }
}


export default NewGameModalContent; 
