import React, { Component } from "react";
import { Modal } from 'react-bootstrap';
import './newGameModal.css';

class NewGameModal extends Component {

  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
    this.openAddGame = this.openAddGame.bind(this);
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeDeveloper = this.handleChangeDeveloper.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      show: false,
      title: '',
      description: '',
      developer: ''
    };
  }

  inputStyle = {
    width: '100%'
  };

  textareaStyle = {
    width: '100%',
    resize: 'none'
  };

  handleClose() {
    this.setState({show: false});
  }

  openAddGame() {
    this.setState({show: true});
  }

  handleChangeTitle(event) {
    this.setState({title: event.target.value})
  }

  handleChangeDescription(event) {
    this.setState({description: event.target.value})
  }

  handleChangeDeveloper(event) {
    this.setState({developer: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault();
    this.addNewGame();
    this.setState({show: false});

    console.log(this.state);
  }
  
  addNewGame() {
    var game = {
      'title': this.state.title,
      'description': this.state.description,
      'developer': this.state.developer
    }

    return fetch('https://localhost:44374/api/games', 
      {
        method: 'POST', 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(game)
      })
      .then(response =>  { 
        response.json();
        this.props.onNewGameAddedChange(game);
      //  this.props.onNewGameAddedChange(response);
      })
      .catch(error => console.error(error));
  }

  render() {

    return (
      <div>
        <div className="addGameSection">
          <button type="button" className="btn btn-secondary" onClick={this.openAddGame}>Add Game</button>
        </div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <form onSubmit={this.handleSubmit}>
            <Modal.Header>
              <Modal.Title>
                Add New Game
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <div>
                  <label>Game title</label>
                  <div>
                    <input type="text" style={this.inputStyle} onChange={this.handleChangeTitle}></input>
                  </div>
                </div>
                <div>
                  <label>Game description</label>
                  <div>
                    <textarea type="text" style={this.textareaStyle} onChange={this.handleChangeDescription}/>
                  </div>
                </div>
                <div>
                  <label>Game developer</label>
                  <div>
                    <input type="text" style={this.inputStyle} onChange={this.handleChangeDeveloper}></input>
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <input type="submit" value="Submit" color="primary" className="btn btn-primary" />
              <button type="button" className="btn btn-secondary" onClick={this.handleClose}>Close</button>
            </Modal.Footer>
          </form>
        </Modal>
      </div>
    )
  };

}

export default NewGameModal; 
