import React, { Component } from 'react';
import './home.css';

class Home extends Component {
  render() {
    return (
      <div>
        <div>
          <div className="home-layout">
            <h1>Game and Music Tracker</h1>
            <p>
              The Game and Music tracker does the following:
            </p>
            <ul>
              <li>
                Display, Add, Delete, and Edit game information
              </li>
              <li>
                Displays music Artist, Album, and Track informtaion
              </li>
            </ul>
          </div> 
        </div>
      </div>
    )
  }
}

export default Home;