import React, { Component } from 'react';
import Layout from './layout';

class App extends Component {

	constructor(props, context) {
		super(props, context);
		this.state = {};
	}

	render() {
		return (
			<div>
				<Layout></Layout>
			</div>
		);
	}
}

export default App;
