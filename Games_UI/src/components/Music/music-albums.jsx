import React, { Component } from "react";
import moment from 'moment'
import "./music-albums.css";

class MusicAlbums extends Component {
  constructor(props) {
		super(props);
    this.state = {
      albums: []
    };
    
    this.generateAlbumElement = this.generateAlbumElement.bind(this);
    this.convertAlbumTime = this.convertAlbumTime.bind(this);
    this.convertAlbumType = this.convertAlbumType.bind(this);
  }

  generateAlbumElement(albums) {
    let element = albums.map((album, index) => {
      return (
        <div key={index} className="row music-album-rows">
          <div className="col-sm-3">
            { album.albumName }
          </div>
          <div className="col-sm-3">
            { moment(album.dateReleased).format('L') }
          </div>
          <div className="col-sm-3">
            { this.convertAlbumType(album.albumType) }
          </div>
          <div className="col-sm-3">
            { this.convertAlbumTime(album.albumDuration) }
          </div>
        </div>
      )
    })
    return element;
  }

  convertAlbumType(at) {
    let type = 'other'

    if (at === 'SA') {
      type = 'Studio Album';
      return type;
    }
    else if (at === 'EP') {
      type = 'Extended Play';
      return type;
    }
    else {
      return type;
    }

  }

  convertAlbumTime(ms) {
    let time = 'time unavailable';
    if (ms > 0) {
      let seconds = ms / 1000;
      let minutes =  Math.floor(seconds / 60).toString();
      let remainder = seconds % 60;
      if (remainder < 10) {
        remainder = '0' + remainder.toString();
      }
      else {
        remainder = remainder.toString();
      }
      time = minutes + ':' + remainder;
      return time;
    }
    else {
      return time;
    }
  }

  render() {
    if (this.props.albums.length > 0) {
      let albumElement = this.generateAlbumElement(this.props.albums);
      if (albumElement.length > 0) {
        return (
          <div>
            <div className="music-albums-layout">
              <div className="row top-row">
                <div className="col-sm-3">
                  <label>Album Name</label>
                </div>
                <div className="col-sm-3">
                  <label>Date Released</label>
                </div>
                <div className="col-sm-3">
                  <label>Album Type</label>
                </div>
                <div className="col-sm-3">
                  <label>Duration</label>
                </div>
              </div> 
              <div>{ albumElement }</div>              
            </div>
          </div>
        )
      }
    }
    else {
      return null;
    }
  };
}

export default MusicAlbums;
