import React, { Component } from "react";
import MusicAlbums from "./music-albums";
import MusicArtists from "./music-artists";
import MusicMembers from "./music-members";
import MusicTracks from "./music-tracks";

class MusicSection extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			artists: [],
			selectedArtist: 0,
			selectedArtistTracks: 0,
			albumList: [],
			membersList: [],
			tracksList: []
		};

		this.getArtistList = this.getArtistList.bind(this);
		this.getAlbumList = this.getAlbumList.bind(this);
		this.getArtistMembersList = this.getArtistMembersList.bind(this);
		this.getAlbumTrackList = this.getAlbumTrackList.bind(this);
		this.handleSelectedArtist = this.handleSelectedArtist.bind(this);
		this.handleSelectedArtistMembers = this.handleSelectedArtistMembers.bind(this);
		this.handleSelectedArtistTracks = this.handleSelectedArtistTracks.bind(this);
	}

	componentDidMount() {
		this.getArtistList();
	}

	getArtistList() {
		fetch('https://localhost:44374/api/artist')
		.then(response => {
			if(response.ok) {
				return response.json();
			}
			else {
				throw new Error(response.statusText);
			}
		})
		.then(data => {
			this.setState({isLoading: false, artists: data });
		})
		.catch(error => {
			this.setState({ artists: [] });
			console.log('Error: ', error);
		})
	}

	getAlbumList(artistId) {
    fetch('https://localhost:44374/api/artist/' + artistId + '/albums/')
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      else {
        throw new Error(response.Error);
      }
    })
    .then(data => {
			this.setState({ albumList: data });
      return data;
    })
    .catch(error => {
      this.setState( { albumList: [] });
      console.log('Error: ', error);
    })
	}
	
	getArtistMembersList(artistId) {
		fetch('https://localhost:44374/api/artist/' + artistId + '/allmembers')
		.then(response => {
			if(response.ok) {
				return response.json();
			}
			else {
				throw new Error(response.Error);
			}
		})
		.then(data => {
			this.setState({ membersList: data });
		})
		.catch(error => {
			this.setState( { membersList: [] });
			console.log('Error: ', error);
		})
	}

	getAlbumTrackList(artistId) {
		fetch('https://localhost:44374/api/artist/' + artistId + '/albums/alltracks')
		.then(response => {
			if(response.ok) {
				return response.json();
			}
			else {
				throw new Error(response.Error);
			}
		})
		.then(data => {
			this.setState({ tracksList: data });
			return data;
		})
		.catch(error => {
			this.setState( { tracksList: [] });
			console.log('Error: ', error);
		})
	}

	handleSelectedArtist(artistId) {
		this.setState({ selectedArtist: artistId });
		this.getAlbumList(artistId);
	}

	handleSelectedArtistMembers(artistId) {
		this.setState({ selectedArtist: artistId });
		this.getArtistMembersList(artistId);
	}

	handleSelectedArtistTracks(artistId) {
		this.setState({ selectedArtistTracks: artistId });
		this.getAlbumTrackList(artistId);
	}

	render() {
		return (
			<div>
				<div className="container-fluid">
					<div className="row">
						<div className="col-sm-6">
							<MusicArtists 
								artistList={this.state.artists}
								selectedArtist={this.handleSelectedArtist}
								selectedArtistMembers={this.handleSelectedArtistMembers}
								selectedArtistTracks={this.handleSelectedArtistTracks}
							/>
							<MusicAlbums albums={this.state.albumList}/>
							<MusicMembers 
								members={this.state.membersList}
							/>
						</div>
						<div className="col-sm-6">
							<MusicTracks 
								tracks={this.state.tracksList}
							/>
						</div>
					</div>
				</div>
			</div>
		)
	};
}

export default MusicSection; 