import React, { Component } from "react";
import './music-tracks.css';

class MusicTracks extends Component {
  constructor(props) {
		super(props);
    this.state = {};

    this.generateTrackList = this.generateTrackList.bind(this);
    this.convertAlbumTime = this.convertAlbumTime.bind(this);
  }
  
  generateTrackList(tracksList) {
    let element = tracksList.map((track, index) => {
      return (
        <tr key={index}>
          <td>{ track.orderId }</td>
          <td>{ track.trackName }</td>
          <td>{ track.albumName }</td>
          <td>{ this.convertAlbumTime(track.durationMs) }</td>
        </tr>
      )
    })

    return element;
  }

  convertAlbumTime(ms) {
    let time = 'time unavailable';
    if (ms > 0) {
      let seconds = ms / 1000;
      let minutes =  Math.floor(seconds / 60).toString();
      let remainder = seconds % 60;
      if (remainder < 10) {
        remainder = '0' + remainder.toString();
      }
      else {
        remainder = remainder.toString();
      }
      time = minutes + ':' + remainder;
      return time;
    }
    else {
      return time;
    }
  }

  render() {
    if (this.props.tracks.length < 1) {
      return null;
    }

    let trackListElement = this.generateTrackList(this.props.tracks);
    return (
      <div>
        <div className="music-tracks-layout">
          <table className="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">Album Order</th>
                <th scope="col">Track Name</th>
                <th scope="col">Album Name</th>
                <th scope="col">Duration</th>
              </tr>
            </thead>
            <tbody>
              { trackListElement }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default MusicTracks;
