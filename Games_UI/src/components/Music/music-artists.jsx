import React, { Component } from "react";
import { Link } from "react-router-dom";
import './music-artists.css';

class MusicArtists extends Component {
	constructor(props) {
		super(props);
    this.state = {};
    
    this.clickArtistAlbumsHandler = this.clickArtistAlbumsHandler.bind(this);
    this.clickArtistMembersHandler = this.clickArtistMembersHandler.bind(this);
    this.clickArtistTracksHandler = this.clickArtistTracksHandler.bind(this);
	}

  generateArtistList(artists) {
    let artistList = artists.map((artist, index) => {
      let url = "/artist/" + artist.artistId;
      return (
        <div key={index} className="row music-artist-rows">
          <div className="col-sm-3">
            <Link to={url}>{artist.artistName}</Link>
          </div>
          <div className="col-sm-3">
            <button 
              type="button" 
              className="btn btn-primary"
              onClick={this.clickArtistMembersHandler}
              value={artist.artistId}
            > 
              Show Members
            </button>
          </div>
          <div className="col-sm-3">
            <button 
              type="button" 
              className="btn btn-primary"
              onClick={this.clickArtistAlbumsHandler}
              value={artist.artistId}
            > 
              Show Albums
            </button>
          </div>
          <div className="col-sm-3">
            <button 
              type="button" 
              className="btn btn-primary"
              onClick={this.clickArtistTracksHandler}
              value={artist.artistId}
            > 
              Show All Tracks
            </button>
          </div>
        </div>
      )
    })
    return artistList;
  }

  clickArtistAlbumsHandler(e) {
    this.props.selectedArtist(e.target.value);
  }

  clickArtistMembersHandler(e) {
    this.props.selectedArtistMembers(e.target.value);
  }

  clickArtistTracksHandler(e) {
    this.props.selectedArtistTracks(e.target.value);
  }

	render() {
    if (this.props.artistList.length === 0) {
      return null
    }
    let artistListElement = this.generateArtistList(this.props.artistList);
		return (
			<div>
        <div className="music-artists-layout">
          { artistListElement }
        </div>
			</div>
		)
	};
}

export default MusicArtists;
