import React, { Component } from "react";
import './music-members.css';

class MusicMembers extends Component {
  constructor(props) {
		super(props);
    this.state = {};
    
    this.generateMemberList = this.generateMemberList.bind(this);
    this.convertPosition = this.convertPosition.bind(this);
    this.convertEndYear = this.convertEndYear.bind(this);
    this.convertType = this.convertType.bind(this);
  }
  
  generateMemberList(members) {
    console.log(members);
    let membersList = members.map((member, index) => {
      return (
        <div key={index} className="row music-members-rows">
          <div className="col-sm-2">
            {member.memberName}
          </div>
          <div className="col-sm-4">
            {this.convertPosition(member.position)}
          </div>
          <div className="col-sm-2">
            {member.startYear}
          </div>
          <div className="col-sm-2">
            {this.convertEndYear(member.endYear)}
          </div>
          <div className="col-sm-2">
            {this.convertType(member.memberType)}
          </div>
        </div>
      )
    })
    return membersList;
  }

  convertEndYear(year) {
    if (year) {
      return year;
    }
    else {
      return 'Present';
    }
  }

  convertPosition(position) {
    if (position) {
      return position;
    }
    else {
      return 'type unknown';
    } 
  }

  convertType(type) {
    if (type === 1) {
      return 'Current Member';
    }
    else if (type === 2) {
      return 'Past Member';
    }
    else if (type === 3) {
      return 'Current Tour Member';
    }
    else if (type === 4) {
      return 'Past Tour Member';
    }
    else return 'unknown';
  }

  render() {

    if (this.props.members.length === 0) {
      return null;
    }

    let membersListElement = this.generateMemberList(this.props.members);

		return (
			<div>
        <div className="music-members-layout">
          <div className="containter-fluid">
            <div className="row top-row"> 
              <div className="col-sm-2">
                Name
              </div>
              <div className="col-sm-4">
                Position
              </div>
              <div className="col-sm-2">
                Start
              </div>
              <div className="col-sm-2">
                End
              </div>
              <div className="col-sm-2">
                Type
              </div>
            </div>
            { membersListElement }
          </div>
          
        </div>
			</div>
		)
	};

}

export default MusicMembers;
