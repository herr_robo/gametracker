using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
using TodoApi.SortWork;

namespace TodoApi.Controllers
{
  [Route("api/sorting")]
  [ApiController]

  public class SortingController: ControllerBase
  {
    private ISortHelper m_sortHelper;
    public SortingController(ISortHelper sortHelper)
    {
      m_sortHelper = sortHelper;
    }
    // Post api/Sorting/Quicksort
    [HttpPost("quicksort")]
    public int[] QuickSort(SortRecord toSort)
    {
      int n = toSort.toSort.Length;
      int[] sorted = m_sortHelper.quickSort(toSort.toSort, 0, n-1);
      return sorted;
    }

    // Post api/Sorting/Mergesort
    [HttpPost("mergesort")]
    public int[] MergeSort(SortRecord toSort)
    {
      int n = toSort.toSort.Length;
      int[] sorted = m_sortHelper.mergeSort(toSort.toSort, 0, n - 1);
      return toSort.toSort;
    }

  }
}
