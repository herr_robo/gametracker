using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
using TodoApi.Repositories;

namespace TodoApi.Controllers
{
    [Route("api/todo")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private IGameRepository m_GameRepository;
        public TodoController(IGameRepository gameRepository)
        {
            m_GameRepository = gameRepository;
        }

        // GET: api/Todo
        //[HttpGet]
        //public object GetTodoItems()
        //{
        //    List<TodoItemRecord> tasksList = new List<TodoItemRecord>();
        //    tasksList = m_GameRepository.GetTasks();
            
        //    if (tasksList.Count == 0)
        //    {
        //        return NotFound();
        //    }
        //    return tasksList;
        //}

/*         // GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoItemRecord>> GetTodoItem(long id)
        {
            var todoItem = await _context.TodoItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }
            
            return todoItem;
        }

        // POST: api/Todo
        [HttpPost]
        public async Task<ActionResult<TodoItemRecord>> PostTodoItem(TodoItemRecord todoItem)
        {
            _context.TodoItems.Add(todoItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTodoItem", new { id = todoItem.Id}, todoItem);
        }

        // PUT: api/Todo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, TodoItemRecord todoItem)
        {
            if (id != todoItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(todoItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/Todo/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TodoItemRecord>> DeleteTodoItem(long id)
        {
            var todoItem = await _context.TodoItems.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            _context.TodoItems.Remove(todoItem);
            await _context.SaveChangesAsync();

            return todoItem;

        } */
    }
}