using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [Route("api/games")]
    [ApiController]

    public class GameController : ControllerBase
    {
        private GamesMusicContext _context;

        public GameController(GamesMusicContext context)
        {
            _context = context;
        }

        // GET: api/Games
        [HttpGet]
        public async Task<IActionResult> GetGames()
        {
            List<Games> games = await _context.Games.ToListAsync();
            return Ok(games);
        }

        // GET: api/Games/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGame(int id)
        {
            Games game = new Games();
            game = await _context.Games.Where(g => g.Id == id).FirstOrDefaultAsync();

            if (game == null)
            {
                return NotFound();
            }
            return Ok(game);
        }

        // POST: api/Games
        [HttpPost]
        public IActionResult AddGame(Games game)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            _context.Games.Add(game);
            _context.SaveChanges();
            return Ok();
        }


        // PUT: api/Games/5
        [HttpPut("{id}")]
        public IActionResult EditGame(int id, Games game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            var existingGame = _context.Games.AsNoTracking().Where(g => g.Id == id).FirstOrDefault<Games>();

            if (existingGame != null)
            {
                _context.Update<Games>(game);
                _context.SaveChanges();
                return Ok();
            }
            else
            {
                return NotFound();
            }

        }

        // DELETE: api/Games/5
        [HttpDelete("{id}")]
        public IActionResult DeleteGame(int id)
        {
            if (id <= 0)
            {
                BadRequest("Not a valid game id");
            }

            // Games game = new Games();
            var game = _context.Games.Where(g => g.Id == id).ToList();

            if (game.Count == 1)
            {
                _context.Remove<Games>(game.First());
                _context.SaveChanges();
                return Ok();
            }
            else
            {
                return NotFound();
            }   
        }     
    }



}

