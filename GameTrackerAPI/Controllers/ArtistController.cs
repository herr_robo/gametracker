using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;
using TodoApi.Repositories;

namespace TodoApi.Controllers
{
  [Route("api/artist")]
  [ApiController]

  public class ArtistController : ControllerBase
  {
    private GamesMusicContext _context;

    public ArtistController(IArtistRepository artistRepository, GamesMusicContext context)
    {
      _context = context;
    }

    // GET: api/Artist
    [HttpGet]

    public async Task<IActionResult> GetArtists()
    {
        List<Artists> artistList = await _context.Artists.ToListAsync();
        return Ok(artistList);
    }

        // GET: api/Artist/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetArtist(int id)
        {
            Artists artist = new Artists();
            artist = await _context.Artists.Where(a => a.ArtistId == id).FirstOrDefaultAsync();

            if (artist == null)
            {
                return NotFound();
            }
            return Ok(artist);
        }

        // GET: api/Artist/5/albums/
        [HttpGet("{id}/albums")]
        public async Task<IActionResult> GetArtistAlbums(int id)
        {
            List<Albums> albums = await _context.Albums.Where(a => a.ArtistId == id).ToListAsync();
            return Ok(albums);
        }

        // GET: api/Artist/5/Albums/4
        [HttpGet("{artistId}/albums/{albumId}")]
        public async Task<IActionResult> GetArtistAlbumTracks(int artistId, int albumId)
        {
            List<Tracks> tracks = await _context.Tracks.Where(t => t.ArtistId == artistId && t.AlbumId == albumId).ToListAsync();
            return Ok(tracks);
        }

        // GET: api/Artists/5/Allmembers
        [HttpGet("{artistId}/allmembers")]
        public async Task<IActionResult> GetArtistAllMembers(int artistId)
        {
            List<ArtistMembers> members = await _context.ArtistMembers.Where(m => m.ArtistId == artistId).ToListAsync();
            return Ok(members);
        }

        // GET: api/Artist/5/Albums/Alltracks
        [HttpGet("{artistId}/albums/alltracks")]
        public async Task<IActionResult> GetArtistAllAlbumTracks(int artistId)
        {
            List<Tracks> tracks = await _context.Tracks.Where(t => t.ArtistId == artistId).ToListAsync();
            return Ok(tracks);
        }

        // GET: api/Artist/5/getAll
        [HttpGet("{id}/getAll")]
        public async Task<IActionResult> GetArtistAllInfo(int id)
        {
            // doing a couple separate LINQ queries for the moment
            var albums = await _context.Albums
                                 .Where(a => a.ArtistId == id)
                                 .Select(a => new { a.AlbumId, a.AlbumName, a.AlbumType, a.AlbumDuration, a.DateReleased, a.AlbumImage }).ToListAsync();


            var members = await _context.ArtistMembers
                                  .Where(m => m.ArtistId == id)
                                  .Select(m => new { m.MemberId, m.MemberName, m.MemberType, m.Position, m.StartYear, m.EndYear }).ToListAsync();

            var tracks = await _context.Tracks
                                       .Where(t => t.ArtistId == id)
                                       .Select(t => new { t.TrackId, t.TrackName, t.DurationMs, t.OrderId } )
                                       .ToListAsync();

            var all = new { albums, members, tracks };

            return Ok(all);

        }
    }

}
