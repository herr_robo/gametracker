﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TodoApi.Models
{
    public partial class GamesMusicContext : DbContext
    {
        public GamesMusicContext()
        {
        }

        public GamesMusicContext(DbContextOptions<GamesMusicContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Albums> Albums { get; set; }
        public virtual DbSet<ArtistMembers> ArtistMembers { get; set; }
        public virtual DbSet<Artists> Artists { get; set; }
        public virtual DbSet<Games> Games { get; set; }
        public virtual DbSet<Tasks> Tasks { get; set; }
        public virtual DbSet<Tracks> Tracks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-COR7TS4\\SQLEXPRESS;Database=GamesMusic;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Albums>(entity =>
            {
                entity.HasKey(e => e.AlbumId)
                    .HasName("PK__Albums__97B4BE3725DA5B50");

                entity.Property(e => e.AlbumName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.AlbumType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DateReleased)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ArtistMembers>(entity =>
            {
                entity.HasKey(e => e.MemberId);

                entity.Property(e => e.MemberId).ValueGeneratedNever();

                entity.Property(e => e.MemberName).HasMaxLength(50);
            });

            modelBuilder.Entity<Artists>(entity =>
            {
                entity.HasKey(e => e.ArtistId)
                    .HasName("PK__Artists__25706B502DA041F9");

                entity.Property(e => e.ArtistName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Games>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.Developer)
                    .IsRequired()
                    .HasColumnName("developer")
                    .HasMaxLength(100);

                entity.Property(e => e.Played).HasColumnName("played");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tasks>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsComplete).HasColumnName("isComplete");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tracks>(entity =>
            {
                entity.HasKey(e => e.TrackId)
                    .HasName("PK__Tracks__7A74F8E044358935");

                entity.Property(e => e.TrackId).ValueGeneratedNever();

                entity.Property(e => e.DurationMs).HasColumnName("Duration_MS");

                entity.Property(e => e.TrackName)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
