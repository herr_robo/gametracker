﻿using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public partial class Games
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Developer { get; set; }
        public int? Played { get; set; }
    }
}
