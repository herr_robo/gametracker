﻿using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public partial class ArtistMembers
    {
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public string Position { get; set; }
        public short? StartYear { get; set; }
        public short? EndYear { get; set; }
        public short? MemberType { get; set; }
        public int ArtistId { get; set; }
    }
}
