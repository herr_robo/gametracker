﻿using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public partial class Tasks
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? IsComplete { get; set; }
    }
}
