﻿using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public partial class Artists
    {
        public int ArtistId { get; set; }
        public string ArtistName { get; set; }
    }
}
