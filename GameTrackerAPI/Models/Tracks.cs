﻿using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public partial class Tracks
    {
        public int TrackId { get; set; }
        public string TrackName { get; set; }
        public int? OrderId { get; set; }
        public int DurationMs { get; set; }
        public int AlbumId { get; set; }
        public int ArtistId { get; set; }
    }
}
