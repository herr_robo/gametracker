﻿using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public partial class Albums
    {
        public int AlbumId { get; set; }
        public string AlbumName { get; set; }
        public string DateReleased { get; set; }
        public int ArtistId { get; set; }
        public string AlbumType { get; set; }
        public int? AlbumDuration { get; set; }
        public string AlbumImage { get; set; }
    }
}
