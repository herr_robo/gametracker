using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApi.Models;

namespace TodoApi.SortWork
{
  public class SortHelper : ISortHelper
  {
    public int[] quickSort(int[] arr, int low, int high)
    {
      if (low < high)
      {
        int pi = partition(arr, low, high);

        quickSort(arr, low, pi -1);
        quickSort(arr, pi + 1, high);
      }
      return arr;
    }

    private static int partition(int[] arr, int low, int high)
    {
      int pivot = arr[high];
      int i = low - 1;

      for (int j = low; j < high; j++)
      {
        if (arr[j] <= pivot)
        {
          i++;
          int temp = arr[i];
          arr[i] = arr[j];
          arr[j] = temp;
        }
      }

      int temp1 = arr[i + 1];
      arr[i + 1] = arr[high];
      arr[high] = temp1;


      return i + 1;
    }

    public int[] mergeSort(int[] arr, int left, int right)
    {
      if (left < right)
      {
        int middle = left + (right - left) / 2;

        mergeSort(arr, left, middle);
        mergeSort(arr, middle + 1, right);

        merge(arr, left, middle, right);
      }

      return arr;
    }

    private int[] merge(int[] arr, int left, int middle, int right)
    {
      int i, j, k;
      int n1 = middle - left + 1;
      int n2 = right - middle;

      int[] L = new int[n1];
      int[] R = new int[n2];

      for (i = 0; i < n1; i++)
      {
        L[i] = arr[left + i];
      }

      for (j = 0; j < n2; j++)
      {
        R[j] = arr[middle + 1 + j];
      }

      i = 0;
      j = 0;
      k = left;

      while (i < n1 && j < n2)
      {
        if (L[i] <= R[j])
        {
          arr[k] = L[i];
          i++;
        }
        else 
        {
          arr[k] = R[j];
          j++;
        }
        k++;
      }

      while (i < n1)
      {
        arr[k] = L[i];
        i++;
        k++;
      }

      while (j < n2)
      {
        arr[k] = R[j];
        j++;
        k++;
      }

      return arr;
    }
  }

}