using System;
using System.Collections.Generic;
using TodoApi.Models;

namespace TodoApi.SortWork
{
  public interface ISortHelper
  {
    int[] quickSort(int[] arr, int low, int high);
    int[] mergeSort(int[] arr, int left, int right);

  }
}